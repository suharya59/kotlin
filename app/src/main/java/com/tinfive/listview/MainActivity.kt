package com.tinfive.listview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //get data array from string.xml
        val sumTesting = getString(R.string.testing_again)
        val firstNames = resources.getStringArray(R.array.first_name)
        val lastNames = resources.getStringArray(R.array.last_name)
        val avatars = resources.getStringArray(R.array.avatar)

//        setDefaultView(firstNames,lastNames, avatars)

        user_item_layout.adapter = UserAdapter(firstNames,lastNames, avatars)
        user_item_layout.layoutManager = LinearLayoutManager(this)

    }

    //Static View
//    private fun setDefaultView(firstNames: Array<String>, lastNames: Array<String>, avatars: Array<String>) {
//        //idTextView = getArray()
//        firstName.text = firstNames[2]
//
//        //concat by kotlin
//        lastName.text = " ${lastNames[2]}"
//
//        //getImageUrl
//        Picasso.get()
//            .load(avatars[2])
//            .placeholder(R.mipmap.ic_launcher)
//            .into(avatar)
//
//    }
}

package com.tinfive.listview

import android.media.Image
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class UserAdapter(val firstNames: Array<String>, val lastNames: Array<String>, val avatars: Array<String>) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item_layout, parent, false) as View

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return firstNames.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.tv_firstName.text = firstNames[position]
        holder.tv_lastName.text = " ${lastNames[position]}"
        Picasso.get()
            .load(avatars[position])
            .placeholder(R.mipmap.ic_launcher)
            .into(holder.iv_avatar)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_firstName = view.findViewById<TextView>(R.id.tv_firstName)
        val tv_lastName = view.findViewById<TextView>(R.id.tv_lastName)
        val iv_avatar = view.findViewById<ImageView>(R.id.iv_avatar)
    }
}
